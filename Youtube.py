from os.path import realpath,dirname,join,exists
from os import mkdir, makedirs
from os import system
from requests import post,get
from pprint import pprint
import re
from bs4 import BeautifulSoup
from time import sleep
import json


FILE_DIR = dirname(realpath(__file__))

class Youtube:
	downloaded_videos = []

	def __init__(self, url, len, name_video):
		self.link_videos = url
		self.folder_filename = join(FILE_DIR, name_video)
		self.folder = name_video

		try:
			makedirs(join(FILE_DIR, self.folder, "videos"))
		except:
			pass

		with open(join(FILE_DIR, self.folder, f"{self.folder}_descargados.txt"),'w') as f:
			pass

		if self.link_videos:
			with open(join(FILE_DIR, self.folder, f"{self.folder}_faltantes.txt"), 'w') as f:
				for url_faltantes in self.link_videos:
					f.write(url_faltantes+'\n')			

		if self.link_videos:
			with open(join(FILE_DIR, self.folder, f"{self.folder}.txt"), 'w') as f:
				for link in self.link_videos:
					f.write(f"{link}\n")

	def do_get_list_video_id(self, url_list):
		self.current_response = get(url_list)
		self.url_list = re.search(r'list=.+$',url_list).group()

	def prepare_list_video_id(self):
		self.list_videos_id = []
		soup = BeautifulSoup(self.current_response.text,'html.parser')
		scripts = soup.find_all('script')

		script = [sc.decode_contents() for sc in scripts if 'var ytInitialData' in sc.decode_contents()][0][20:-1]

		# self.innertube_api_key = re.search(r'"INNERTUBE_API_KEY":"[^\"]+",',self.current_response.text)


		list_videos_id = set(re.findall(r'"videoId":".{11}",', script))
		
		for id in list_videos_id:
			id_decode = re.search(r'".{11}"', id).group().replace('"','')
			self.list_videos_id.append(id_decode)

	def prepare_list_url(self):
		self.link_videos = []
		url_base = 'https://www.youtube.com/watch?v='
		for id_video in self.list_videos_id:
			self.link_videos.append(url_base+id_video+self.url_list)
		
		with open(join(FILE_DIR,f"{self.folder}.txt"), 'w') as f:
			for link in self.link_videos:
				f.write(f"{link}\n")

		with open(join(FILE_DIR,f"{self.folder}_faltantes.txt"), 'a') as f:
			for url_faltantes in self.link_videos:
				f.write(url_faltantes+'\n')
		# if self.innertube_api_key:
		# 	self.innertube_api_key = self.innertube_api_key.group().split(':')[1][1:-2]
		# 	url_extra_video_id = f'https://www.youtube.com/youtubei/v1/browse?key={self.innertube_api_key}'
		# 	self.current_response = post(url_extra_video_id)		

	def _prepare_query(self,url):
		self.data = {
			'q': url,
			'vt': 'home'
		}

	def do_post_query(self):
		url = '	https://yt1s.com/api/ajaxSearch/index'
		self.current_response = post(url, data=self.data)


	def _prepare_url_download(self):
		self.title = self.current_response.json()['title']
		self.data = {
			'vid' : self.current_response.json()['vid'],
			'k': self.current_response.json()['links']['mp4']['auto']['k']
		}

	def do_post_url_download(self):		
		url = 'https://yt1s.com/api/ajaxConvert/convert'
		self.current_response = post(url, data=self.data)

	def do_get_download_video(self):
		url = self.current_response.json()['dlink']
		self.current_response = get(url, stream=True)

	def save_video(self):
		with open(join(self.folder_filename, 'videos', self.title.replace('/',' ').replace('"',"'")+'.mp4'), 'wb') as f:
			for chunk in self.current_response.iter_content(chunk_size=1024):
				f.write(chunk)

	def prepare_link_list_reproduction(self):
		self.do_get_list_video_id(input('Inserte el link de la lista de reproducción: '))
		self.prepare_list_video_id()
		self.prepare_list_url()

	def run(self):
		try:
			mkdir(self.folder)
		except:
			pass		

		calidades = [
			'22',
			'auto',
			'135',
			'18',
			'133',
			'160'
		]
		_error_ = []

		for i, url in enumerate(self.link_videos):
			with open(join(FILE_DIR,self.folder, f"{self.folder}_descargados.txt"),'r') as f:
				in_folder = not url+'\n' in f.readlines()

			if in_folder:
				try:
					print('-'*70)
					print('Preparando consulta')
					self._prepare_query(url)
					self.do_post_query()
					self._prepare_url_download()
					self.do_post_url_download()
					self.do_get_download_video()
					print(f'Descargando video - {self.title}')
					print('Se accedió al código binario')
					self.save_video()
					print('Descarga exitosa')
					if url == self.link_videos[len(self.link_videos)-1]:
						print('-'*70)
				except:
					_error_.append(url)
					with open(join(FILE_DIR, self.folder, f"{self.folder}_fallidas.txt"), 'a') as f:
						f.write(f"{url}\n")

					print('Uy Algo Paso')
					# raise
		
				with open(join(FILE_DIR, self.folder, f"{self.folder}_descargados.txt"),'a') as f:
					f.write(f"{url}\n")

				with open(join(FILE_DIR, self.folder, f"{self.folder}_faltantes.txt"), 'w') as f:
					for url_faltante in self.link_videos[i:]:
						f.write(f"{url_faltante}\n")

		self.link_videos = _error_
		print('FIN DEL LAS DESCARGAS')
		# sleep(60)
		# system('systemctl poweroff')