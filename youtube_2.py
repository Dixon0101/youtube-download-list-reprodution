from requests import Session
from traceback import format_exc


class Youtube(Session):
	HOST = 'yt1s.com'
	BASE_URL = f"https://{HOST}"

	def __init__(self, url, directory_instance, quality,*args, **kwargs) -> None:
		super(Youtube, self).__init__(*args, **kwargs)
		self.directory_instance = directory_instance
		self.url = url
		self.quality = str(quality)

		self.headers.update({
			'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0'
		})

	def _prepare_do_post_index(self):
		form = {
			'q': self.url,
			'vt': 'mp4'
		}

		return form

	def _do_post_index(self, form):
		'''
        Network File
        ------------
           index (POST)
        '''
		url = f"{self.BASE_URL}/api/ajaxSearch/index"

		self.current_response = self.post(url, data=form)

	def _prepare_do_post_convert(self):
		json_response = self.current_response.json()

		form = {
			'vid': json_response['vid'],
		}
		link_mp4 = json_response['links']['mp4']

		for i, item in link_mp4.items():
			if self.quality in item['q']:
				form['k'] = item['k']
				break
		
		return form

	def _do_post_convert(self, form):
		'''
        Network File
        ------------
           convert (POST)
        '''
		url = f"{self.BASE_URL}/api/ajaxConvert/convert"

		self.current_response = self.post(url, data=form)

	def _extract_url(self):
		url = self.current_response.json()['dlink']
		return url

	def _do_get_dowload_video(self, url):
		self.current_response = self.get(url, stream=True)

	def _save_video(self):
		for chunk in self.current_response.iter_content(chunk_size=1024):
			self.directory_instance.write(chunk)

	def run(self):
		try:
			form = self._prepare_do_post_index()
			self._do_post_index(form)
			form = self._prepare_do_post_convert()
			self._do_post_convert(form)
			url = self._extract_url()
			self._do_get_dowload_video(url)
			self._save_video()
		except:
			print(format_exc(chain=False))
